#include "TimeTest.h"
#include "Logic.h"
#include "GPU.h"
#include "Windows.h"
#include <iostream>

TimeTest::TimeTest()
{
}


TimeTest::~TimeTest()
{
}

std::string TimeTest::timeForNGenerationsCPU(int N)
{
	//double totalTime = 0;
	double totalCalculationTime = 0;
	Logic logic = Logic();

	while (logic.getGeneration() != N+1) 
	{
		//int before = GetTickCount();

		logic.performNextStepCPU();

		//int after = GetTickCount();
		//double t1 = after - before;
		//totalTime += t1 / 1000;
		double t2 = logic.getTimePerGeneration();
		totalCalculationTime += t2 / 1000;
	}
	std::string message;
	//std::string str1 = "|Total time CPU: ";
	std::string str2 = "|Calculation time CPU: ";
	//message.append(str1);
	//message.append(std::to_string(totalTime));
	//message.append("s ");
	message.append(str2);
	message.append(std::to_string(totalCalculationTime));
	message.append("s| \n");

	std::cout << "N= " << N << std::endl;

	return message;
}

std::string TimeTest::timeForNGenerationsGPU(int N)
{
	//double totalTime = 0;
	double totalCalculationTime = 0;
	Logic logic = Logic();
	GPU gpu = GPU();

	while (logic.getGeneration() != N + 1)
	{
		//int before = GetTickCount();

		logic.setVecAsMain(gpu.generateNextVec(logic.getVecSize(), logic.getBoardWidth(), logic.getBoardVec()));
		logic.iterateGeneration();
		gpu.cleanUpMemory();

		//int after = GetTickCount();
		//double t1 = after - before;
		//totalTime += t1 / 1000;
		double t2 = gpu.getTimePerGeneration();
		totalCalculationTime += t2 /1000 ;
	}
	gpu.fullCleanUp();
	double nanoTomilis = (double) totalCalculationTime;

	std::string message;
	//std::string str1 = "|Total time GPU: ";
	std::string str2 = "|Calculation time GPU: ";
	//message.append(str1);
	//message.append(std::to_string(totalTime));
	message.append("s ");
	message.append(str2);
	message.append(std::to_string(totalCalculationTime));
	message.append("mks \n"); // mikroseconds

	std::cout << "N= " << N << std::endl;

	return message;
}
