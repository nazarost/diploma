#pragma once
#include <string>

class TimeTest
{
public:
	TimeTest();
	~TimeTest();
	std::string timeForNGenerationsCPU(int N);
	std::string timeForNGenerationsGPU(int N);
};

