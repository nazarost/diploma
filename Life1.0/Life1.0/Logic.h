#pragma once
class Logic
{
	
private:
	int BOARD_WIDTH;
	int BOARD_HEIGTH;
	int generation=0;
	int timePerGeneration;
	char **board;
	

public:
	//constructors
	Logic(int height, int width);
	~Logic(); 
	// board parameters
	void setBoardHeigth(int H);
	void setBoardWidth(int W);
	int getBoardHeigth();
	int getBoardWidth();

	void iterateGeneration();
	int getGeneration();
	void setTimePerGeneration(long int time);
	long int getTimePerGeneration();
	// board manipulations
	char **generateEmptyBoard(int H, int W);
	void setBoardAsMain(char **inputBoard);
	char **getBoard();
	void deleteBoard();

	//void printBoard();

	void performNextStep();
	// oparrations with cells
	void fillByDeadCells();
	int checkCell(int x, int y);
	int Logic::checkNeighbors(int x, int y);
	bool isAlive(int x, int y);
	char **generateNextBoard();
	//seeding
	void seedFigure1();
	void testSeed();
	void seedRandomArea();
};

