#include "stdafx.h"
#include "GUI.h"
#include "Logic.h"
#include <Windows.h>
#include <iostream>
#include <string>

#include <SFML/Graphics.hpp>


GUI::GUI(Logic logic, char ** inputBoard, int boardWidth, int boardHeight)
{
	this->boardWidth = boardWidth;
	this->boardHeight = boardHeight;
	if ((boardWidth <= 100) || (boardHeight <= 100)) {
		cellSize = 5;
		distance += cellSize;
	}
	if (((boardWidth > 100) || (boardHeight > 100)) && ((boardWidth <= 200) || (boardHeight <= 200)) ){
		cellSize = 2;
		distance += cellSize;
	}
	if ((boardWidth > 200) || (boardHeight > 200)) {
		cellSize = 1;
		distance = cellSize;
	}
	drawBoard(logic);
}


GUI::~GUI()
{
}

void GUI::drawBoard(Logic logic)
{
	sf::RenderWindow window(sf::VideoMode(boardWidth*distance, boardHeight*distance + placeForText), "Life");
	sf::RectangleShape aliveCell(sf::Vector2f(cellSize, cellSize));
	sf::RectangleShape deadCell(sf::Vector2f(cellSize, cellSize));

	sf::Text text1;
	sf::Text text2;
	text2.setPosition(0,11);
	sf::Font font;
	font.loadFromFile("arial.ttf");
	text1.setFont(font);
	text2.setFont(font);

	std::string curGenStr = "Current generation: ";
	std::string timeStr = "Time of step generation: ";

	std::string genNum = std::to_string(logic.getGeneration());
	std::string timeNum = std::to_string(logic.getTimePerGeneration());
	std::string milis = " ms";

	std::string firstLine ="";
	std::string secondLine = "";

	text1.setString(firstLine.append(curGenStr).append(genNum));
	text1.setCharacterSize(11);
	text1.setFillColor(sf::Color::White);

	text2.setString(secondLine.append(timeStr).append(timeNum).append(milis));
	text2.setCharacterSize(11);
	text2.setFillColor(sf::Color::White);

	sf::Vertex line[2] =
	{
		sf::Vertex(sf::Vector2f(0.f, placeForText)),
		sf::Vertex(sf::Vector2f(boardWidth*distance, placeForText))
	};

	aliveCell.setFillColor(alive);
	deadCell.setFillColor(dead);

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}

		window.clear();
		for (int i = 0; i<boardHeight; ++i) {
			for (int j = 0; j<boardWidth; ++j) {
				if (logic.isAlive(i, j)) {
					aliveCell.setPosition(j*distance, i*distance + placeForText);
					window.draw(aliveCell);
				}
				else
				{
					deadCell.setPosition(j*distance, i*distance + placeForText);
					window.draw(deadCell);
				}
			}
		}

		genNum = std::to_string(logic.getGeneration());
		timeNum = std::to_string(logic.getTimePerGeneration());

		firstLine = "";
		secondLine = "";

		text1.setString(firstLine.append(curGenStr).append(genNum));
		text2.setString(secondLine.append(timeStr).append(timeNum).append(milis));

		window.draw(text1);
		window.draw(text2);
		window.draw(line, 2, sf::Lines);
		
		window.display();
		//Sleep(50); //windows.h
		logic.performNextStep();
	}
}
